const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const { version } = require('./package.json');

const html_webpack_plugin = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: './index.html',
});

const definitions = new webpack.DefinePlugin({
  __VERSION__: JSON.stringify(version),
  __GA_UID__: JSON.stringify('UA-158864468-2'),
});

module.exports = {
  entry: ['./src/stylesheets/main.scss', './src/javascript/index.tsx'],
  output: {
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
        },
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      }, {
        test: /\.(scss|css)$/,
        use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
      }, {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader?name=images/[name].[hash].[ext]',
      }, {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=fonts/[name].[hash].[ext]',
      },
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "ts-loader",
          options: {
            transpileOnly: false, // Set to true if you are using fork-ts-checker-webpack-plugin
            projectReferences: true
          }
        }
      }
    ],
  },
  plugins: [
    html_webpack_plugin,
    definitions,
  ],
  resolve: {
    extensions: ['.ts','.tsx','.js', '.jsx', '.scss'],
  },
};