import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { CssBaseline } from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import LoginPage from "../components/Login";
import MainPage from "../components/Main";

import { initialState, IState } from '../redux/initialState';
import storeCreator from '../redux/store';

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql
} from "@apollo/client";

function saveAppSelections(state: IState) {
  try {
    const savedState = JSON.stringify(state);

    localStorage.setItem('luxor-pokemon', savedState);
  } catch (e) {
    console.log(e);
  }
}

function loadSavedAppSelections() {
  try {
    const savedState = localStorage.getItem('luxor-pokemon');

    if (savedState !== null) {
      return JSON.parse(savedState);
    }
    return initialState;
  } catch (e) {
    console.log(e);
    return initialState;
  }
}

const persistedState = loadSavedAppSelections();
const store = storeCreator(persistedState);
store.subscribe(() => saveAppSelections(store.getState()));

const client = new ApolloClient({
  uri: 'https://graphql-pokemon2.vercel.app/',
  cache: new InMemoryCache()
});

ReactDOM.render(
  <Provider store={store}>
    <CssBaseline />
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route exact path="/main" component={MainPage} />
          <Route exact path="/" component={LoginPage} />
        </Switch>
      </Router>
    </ApolloProvider>,
  </Provider>,
  document.querySelector('#app'),
);