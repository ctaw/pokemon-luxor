import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { authenticate } from '../actions/authenticate';

import { IState } from '../redux/initialState';

function App() {
  const dispatch = useDispatch();
  const { authenticated } = useSelector((state: IState) => state.authenticate);
  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const loginUser = () => {
    if (userName === 'admin' && password ==='admin') {
      dispatch(authenticate({ authenticated: true }));
    }
  }

  console.log('authenticated: ', authenticated);
  return (
    <div>
      <input type="text" onChange={(event) => setUsername(event.target.value)} value={userName} />
      <input type="password" onChange={(event) => setPassword(event.target.value)} value={password} />
      <button onClick={loginUser}>login</button>
    </div>
  );
}

export default App;