import * as types from '../redux/action-types';

type PokemonSize = {
    minimum: string;
    maximum: string;
}

interface PokemonDetails {
  id: string;
  number: string;
  name: string;
  image: string;
  classification: string;
  weight: PokemonSize
  height: PokemonSize
}

export function updatePokemonList(pokemons: []) {
  return {
    type: types.UPDATE_POKEMON_LIST,
    payload: pokemons,
  };
}

export const set_pokemon = (pokemon: any) => ({ type: types.SET_POKEMON, payload: pokemon });