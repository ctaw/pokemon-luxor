import * as types from '../redux/action-types';

type AuthenticatedValue = {
  authenticated: boolean;
}

export function authenticate(value: AuthenticatedValue){
  return {
    type: types.AUTHENTICATE,
    payload: value,
  };
}

export const logout = () => ({ type: types.LOGOUT, payload: null });