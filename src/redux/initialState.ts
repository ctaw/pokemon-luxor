type AuthenticateState = {
  authenticated: boolean;
};

export interface IState {
  authenticate: AuthenticateState;
  pokemons: any;
}

export const initialState: IState = {
  authenticate: {
    authenticated: false,
  },
  pokemons: {
    pokemon: {},
    list: []
  },
};