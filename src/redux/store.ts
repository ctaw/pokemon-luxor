/* eslint-disable no-underscore-dangle */
import { applyMiddleware, compose, createStore } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import ReduxThunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';

const middleware = [reduxImmutableStateInvariant(), ReduxThunk];

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__?: typeof compose;
  }
}


export default (persistedState: {} | undefined) => {
  if (process.env.NODE_ENV === 'production' || !window.__REDUX_DEVTOOLS_EXTENSION__) {
    return createStore(rootReducer, persistedState, applyMiddleware(ReduxThunk));
  }

  return createStore(rootReducer, persistedState, compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    
  ));
};
