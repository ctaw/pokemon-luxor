import * as types from '../action-types';
import { initialState } from '../initialState';

export default (state = initialState.pokemons, action: any) => {
  switch (action.type) {
    case types.UPDATE_POKEMON_LIST:
      return {
        ...state,
        list: action.payload,
      };
    
      case types.SET_POKEMON:
        return {
          ...state,
          pokemon: action.payload,
        };
    
  default:
    return state;
  }
};
