import * as types from '../action-types';
import { initialState } from '../initialState';

export default (state = initialState.authenticate, action: any) => {
  switch (action.type) {
    case types.AUTHENTICATE:
      return {
          authenticated: action.payload.authenticated
      };

    case types.LOGOUT:
      return initialState.authenticate;
    
  default:
    return state;
  }
};
