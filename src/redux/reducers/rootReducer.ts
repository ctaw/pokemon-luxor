import { combineReducers } from 'redux';
import authenticate from './authenticationReducer';
import pokemons from './pokemonsReducer';

const rootReducer = combineReducers({
  authenticate,
  pokemons
});

export default rootReducer;
