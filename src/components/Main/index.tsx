import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { IState } from '../../redux/initialState';
import { set_pokemon } from '../../actions/pokemons';
import { logout } from '../../actions/authenticate';
import { gql, useQuery } from '@apollo/client';
import { Container, Grid, Paper, Avatar, Card, CardContent, 
  CardHeader, CardMedia, Box, Typography, Modal,
  Button, Chip, Dialog, DialogActions, DialogContent, DialogTitle} from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import Pagination from '@material-ui/lab/Pagination';

const GET_POKEMON = gql`
  query pokemons($first: Int!){
    pokemons(first: $first){
      id
      number
      name
      image
      classification
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
    }
  }
`;

function Main() {
  const dispatch = useDispatch();
  const [selectedPokemon, setSelectedPokemon] = useState();
  const [open, setOpen] = useState(false);
  const [numberOfPokemons, setNumberOfPokemons] = useState(10);
  const [pokemonList, setPokemonList] = useState([]);
  const [page, setPage] = useState(1);
  const { authenticated } = useSelector((state: IState) => state.authenticate);
  const { pokemon } = useSelector((state: IState) => state.pokemons);
  const limit = 10;
  
  const { loading, data: pokemonData, refetch } = useQuery(GET_POKEMON, {
    variables: { first: numberOfPokemons },
    onCompleted: (data) => {
      setPokemonList(data.pokemons.slice(0, 10));
    }
  });

  useEffect(() => {
    if (numberOfPokemons < 40 && !loading) {
      setNumberOfPokemons(numberOfPokemons + limit);
      refetch()
    }
  }, [numberOfPokemons, loading]);

  const pokemonChange = (obj: any) => {
    if (obj != null) {
      setSelectedPokemon(obj.number);
      dispatch(set_pokemon({ obj }));
    }
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogout = () => {
    dispatch(logout());
    logout();
    window.history.replaceState({}, document.title, '/');
  };

  const paginate = (event: any, page: any) => {
    setPage(page);
    setPokemonList(pokemonData.pokemons.slice(page * 10 - 10, page * 10));
  }

  if (!authenticated) {
    return <Redirect to="/" />
  } else {
    document.body.style.backgroundColor = "#484D57";
  }

  if (loading || numberOfPokemons < 40) {
    return <div className="loader"></div>;
  }


  return (
    <Container className="main-component">
      <Paper className="pokedex">
        <Grid container spacing={0} direction="row" alignItems="center" justifyContent="center">
          <Grid item xs={4} className="pokemon-list">
            {pokemonList.map((pokemon: any) => {
              return (
                <Card key={pokemon.id} onClick={() => pokemonChange(pokemon)} className={selectedPokemon === pokemon.number ? "pokemon-card active" : "pokemon-card"}>
                  <CardContent>
                    <Box sx={{display: 'flex', flexDirection: 'row',}}>
                      <Avatar alt={pokemon.name} src={pokemon.image} />
                      <div className="pokemon-recap">
                        <span className="pokemon-number">{pokemon.number}</span>
                        <span className="pokemon-name">{pokemon.name}</span>
                      </div>
                    </Box>
                  </CardContent> 
                </Card>
              )
            })}
            <Box className="controls">
              <Button
                  color="primary"
                  startIcon={<ExitToAppIcon />}
                  onClick={handleLogout}
                ></Button>
                <Pagination
                  count={4}
                  page={page}
                  onChange={paginate}
                  shape="rounded"
                  className="right pokemon-pagination"
                />
            </Box>
          </Grid>
          <Grid item xs={8}>
          { pokemon.obj != null && (
            <Card className="pokemon-details">
              <CardHeader title={ pokemon.obj.name } 
                          action={"#" + pokemon.obj.number}/>
              
              <Box className="wrapper-details">
                <CardContent className="pokemon-bio">
                  
                  <Grid container justifyContent="center" spacing={2}>
                    <Grid item xs={12} sm={12}>
                      <Typography variant="h6" gutterBottom>
                        <EqualizerIcon/> <span>{ pokemon.obj.name + "'s Stats"}</span>
                     </Typography>
                    </Grid>
                  </Grid>
                  <hr/>

                  <Grid container justifyContent="center" spacing={2} className="pokemon-stats">
                  
                    <Grid item xs={12} sm={6}>
                      <Box className="stats">
                        <Typography variant="overline" display="block" gutterBottom>
                          Min Height
                        </Typography>
                        <Typography variant="button" display="block" gutterBottom>
                          { pokemon.obj.height.minimum }
                        </Typography>
                      </Box>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Box className="stats">
                        <Typography variant="overline" display="block" gutterBottom>
                          Max Height
                        </Typography>
                        <Typography variant="button" display="block" gutterBottom>
                          { pokemon.obj.height.maximum }
                        </Typography>
                      </Box>
                    </Grid>


                    <Grid item xs={12} sm={6}>
                      <Box className="stats">
                        <Typography variant="overline" display="block" gutterBottom>
                          Min Weight
                        </Typography>
                        <Typography variant="button" display="block" gutterBottom>
                          { pokemon.obj.weight.minimum }
                        </Typography>
                      </Box>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <Box className="stats">
                        <Typography variant="overline" display="block" gutterBottom>
                          Max Weight
                        </Typography>
                        <Typography variant="button" display="block" gutterBottom>
                          { pokemon.obj.weight.maximum }
                        </Typography>
                      </Box>
                    </Grid>
                  </Grid>


                  <Grid container justifyContent="center" spacing={2}>
                    <Grid item xs={12} sm={12}>
                      <Chip color="primary" label={ pokemon.obj.classification } />
                    </Grid>
                  </Grid>

                  <Grid container justifyContent="center" spacing={2}>
                    <Grid item xs={12} sm={12}>
                      <Typography variant="caption" className="pokemon-id-small" display="block" gutterBottom>
                        {"ID: "+ pokemon.obj.id }
                      </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
                <CardMedia
                    onClick={handleOpen}
                    image={ pokemon.obj.image }
                    title={ pokemon.obj.name }
                  />

                <Dialog
                  open={open}
                  onClose={handleClose}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">{ pokemon.obj.name }</DialogTitle>
                  <DialogContent>
                    <img src={pokemon.obj.image} />
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose} color="primary">
                      Close
                    </Button>
                  </DialogActions>
                </Dialog>



              </Box>
            </Card>
          )}
          </Grid>
        </Grid>
      </Paper>
    </Container>
  );
}

export default Main;