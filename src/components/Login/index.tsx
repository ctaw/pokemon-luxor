import React, { useState } from 'react';
import { Container, Grid, makeStyles, Paper, Button, TextField, Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useDispatch, useSelector } from 'react-redux';
import { authenticate } from '../../actions/authenticate';
import { IState } from '../../redux/initialState';
import { Redirect, useHistory } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  root: {
    '& .MuiInputLabel-root': {
      color: '#a9a9a9',
    },
    '& .MuiFilledInput-root': {
      background: '#3F414B',
      width: '380px',
      height: '60px',
      color: 'white'
    },
    '& label.Mui-focused': {
      color: 'white'
    },
    '& .MuiFilledInput-underline:after': {
      borderBottomColor: 'white'
    }
  },
  loginForm: {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    boxSizing: 'border-box',
    width: '508px',
    backgroundColor: '#2D2F36',
    borderRadius: '8px',
    padding: '70px 0',
  },
  primary: {
    backgroundColor: '#F2C94C',
    borderRadius: '6px',
    width: '380px',
    height: '60px',
    color: '#1c1d20',
    '&:hover': {
      backgroundColor: '#d3ac35',
      color: 'white',
    }
  },
  loginContainer: {

  }
}));

function Login() {
  const dispatch = useDispatch();
  const { authenticated } = useSelector((state: IState) => state.authenticate);
  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const history = useHistory();

  const loginUser = () => {
    if (userName === 'admin' && password ==='admin') {
      dispatch(authenticate({ authenticated: true }));
      history.push("/main");
    } else {
      setError(true);
      setErrorMessage('Invalid Email and Password!');
    }
  }

  if (authenticated) {
    return <Redirect to="/main" />
  }

  const classes = useStyles();
  document.body.style.backgroundColor = "#1C1D1F";
  return (
    <div className="login">
      <Container className={classes.loginContainer}>
        <Paper className={classes.loginForm}>
          <Grid container spacing={4} direction="column" alignItems="center" justifyContent="center">
            <Grid item xs={12}>
              <TextField
                required
                id="outlined-required"
                label="Username"
                fullWidth
                InputLabelProps={{ shrink: true }}
                classes={{
                  root: classes.root
                }}
                variant="filled"
                onChange={(event) => setUsername(event.target.value)} value={userName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="outlined-password-input"
                label="Password"
                type="password"
                fullWidth
                variant="filled"
                InputLabelProps={{ shrink: true }}
                classes={{
                  root: classes.root
                }}
                autoComplete="current-password"
                onChange={(event) => setPassword(event.target.value)} value={password}
              />
            </Grid>
            <Grid item xs={12}>
              <Button fullWidth className={classes.primary} size="large" onClick={loginUser}> Login </Button>
            </Grid>
          </Grid>
        </Paper>
      </Container>
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        autoHideDuration={10000}
        open={error}
      >
        <Alert
          severity="error"
          variant="filled"
        >
          {errorMessage}
        </Alert>
      </Snackbar>
    </div>
  );
}

export default Login;