const { merge } = require('webpack-merge');
const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval-source-map',
  output: {
    filename: 'app.[hash].js',
    publicPath: '/',
  },
  devServer: {
    port: 4008,
    inline: true,
    disableHostCheck: true,
    contentBase: './dist',
    historyApiFallback: true,
    publicPath: '/',
  },
});
