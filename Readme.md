# Pokedex

### Built With

Major frameworks/libraries used in this project

* [React.js](https://reactjs.org/)
* [Material UI](https://mui.com/)
* [Typescript](https://www.typescriptlang.org/)

## Requirements

1. Node version 14.17.1
2. NPM version 6^
3. Webpack 4^

## Setup

### 1. Clone the repository

```
    git clone git@gitlab.com:ctaw/pokemon-luxor.git
```

### 2. Install all the npm packages

```
    npm install
```

### 2. Run the application but use NODE version 14 ( I have nvm)

``` 
    nvm use 14 ( to change the version )
    npm start
```

## OUTPUT [ GIF ]

![Alt Text](http://g.recordit.co/nVhCEvYZI4.gif)


## PAGINATION

I just included the pagination [ my approach ]

![Alt Text](http://g.recordit.co/AUw6TxVBUf.gif)
